package net.sourceforge.rscbundlecheck.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dyndns.fichtner.rsccheck.engine.Context;
import org.dyndns.fichtner.rsccheck.engine.ErrorEntry;
import org.dyndns.fichtner.rsccheck.engine.RscBundleCheck;
import org.dyndns.fichtner.rsccheck.engine.RscBundleReaderFile;
import org.dyndns.fichtner.rsccheck.engine.StaticVisitorFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

public class SampleBuilder extends IncrementalProjectBuilder {

	private class RscBundleDeltaVisitor implements IResourceDeltaVisitor {
		public boolean visit(final IResourceDelta delta) throws CoreException {
			final IResource resource = delta.getResource();
			switch (delta.getKind()) {
			case IResourceDelta.ADDED:
				// handle added resource
				checkResourcebundle(resource);
				break;
			case IResourceDelta.REMOVED:
				// handle removed resource
				checkResourcebundle(resource);
				break;
			case IResourceDelta.CHANGED:
				// handle changed resource
				checkResourcebundle(resource);
				break;
			}
			// return true to continue visiting children.
			return true;
		}
	}

	private class RscBundleResourceVisitor implements IResourceVisitor {
		public boolean visit(final IResource resource) {
			checkResourcebundle(resource);
			// return true to continue visiting children.
			return true;
		}
	}

	public static final String BUILDER_ID = "net.sourceforge.rscbundlecheck.sampleBuilder";

	private static final String MARKER_TYPE = "net.sourceforge.rscbundlecheck.xmlProblem";

	private static void addMarker(final IFile file, final String message,
			int lineNumber, final int severity) {
		try {
			final IMarker marker = file.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			if (lineNumber == -1) {
				lineNumber = 1;
			}
			marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
		} catch (final CoreException e) {
			e.printStackTrace();
		}
	}

	private IJavaProject javaProject;

	protected IProject[] build(final int kind, final Map args,
			final IProgressMonitor monitor) throws CoreException {
		this.javaProject = JavaCore.create(getProject());
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} else {
			final IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(monitor);
			} else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	private void checkResourcebundle(final IResource resource) {
		try {
			if (resource.getType() == IResource.FILE
					&& resource.getFileExtension() != null
					&& resource.getFileExtension().equalsIgnoreCase(
							"properties")) { //$NON-NLS-1$
				final IFile file = (IFile) resource;
				if (isResourceBundle(file)) {
					final List variants = findVariants(file);
					final Context context = createConfiguration(variants);
					final RscBundleCheck bundleCheck = new RscBundleCheck(
							context);

					for (final Iterator iter = variants.iterator(); iter
							.hasNext();) {
						deleteMarkers((IFile) iter.next());
					}

					final List errors = bundleCheck.execute();
					for (final Iterator iter = errors.iterator(); iter
							.hasNext();) {
						final ErrorEntry error = (ErrorEntry) iter.next();
						final IFile resourcebundle = (IFile) variants
								.get(context.rscBundleReaders.indexOf(error
										.getBundleReader()));
						addMarker(resourcebundle, error.getMessage(), error
								.getEntry() == null ? -1 : error.getEntry()
								.getLineOfKey(), IMarker.SEVERITY_WARNING);
					}
				}
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isResourceBundle(final IFile file)
			throws JavaModelException {
		// File must resist in a source directory
		if (this.javaProject == null) {
			return false;
		}

		final List sourceFolders = getSourceFolders(this.javaProject);
		for (final Iterator iter = sourceFolders.iterator(); iter.hasNext();) {
			final IClasspathEntry entry = (IClasspathEntry) iter.next();
			if (entry.getPath().isPrefixOf(file.getFullPath())) {
				return true;
			}
		}
		return false;
	}

	private static List getSourceFolders(final IJavaProject javaProject)
			throws JavaModelException {
		final IClasspathEntry[] classpathEntries = javaProject
				.getResolvedClasspath(true);
		final List result = new ArrayList(classpathEntries.length);
		for (int i = 0; i < classpathEntries.length; i++) {
			final IClasspathEntry entry = classpathEntries[i];
			if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
				result.add(entry);
			}
		}
		return result;
	}

	private static Context createConfiguration(final List variants)
			throws FileNotFoundException, UnsupportedEncodingException,
			CoreException {
		final Context context = new Context();
		context.visitors = new StaticVisitorFactory().getDefaultVisitors();

		final List readers = new ArrayList(variants.size());
		for (final Iterator iter = variants.iterator(); iter.hasNext();) {
			final IFile iFile = (IFile) iter.next();
			final File file = iFile.getLocation().toFile();
			readers.add(new RscBundleReaderFile(new InputStreamReader(
					new FileInputStream(file), iFile.getCharset()), file
					.toString()));
		}

		context.rscBundleReaders = readers;
		return context;
	}

	public static List findVariants(final IFile file) throws CoreException {
		final String referenceExtension = file.getFileExtension();
		final String referenceExtlessName = getExtlessName(file);
		final int pos = referenceExtlessName.lastIndexOf('_');
		final String refBaseName = pos >= 0 ? referenceExtlessName.substring(0,
				pos) : referenceExtlessName;
		final List fileList = new ArrayList();
		file.getProject().findMember(
				file.getProjectRelativePath().makeAbsolute()
						.removeLastSegments(1)).accept(new IResourceVisitor() {
			public boolean visit(final IResource resource) throws CoreException {
				if (resource instanceof IFile) {
					final IFile thisFile = (IFile) resource;
					if (thisFile.getName().equalsIgnoreCase(
							refBaseName + '.' + referenceExtension)
							|| getExtlessName(thisFile).toLowerCase()
									.startsWith(refBaseName.toLowerCase())
							&& thisFile.getFileExtension().toLowerCase()
									.endsWith(referenceExtension.toLowerCase())) {
						fileList.add(resource);
					}
				}
				return true;
			}
		}, IResource.DEPTH_ONE, false);
		return fileList;
	}

	private static String getExtlessName(final IFile file) {
		final String ext = file.getFileExtension();
		return ext == null || ext.length() == 0 ? file.getName() : file
				.getName().substring(0,
						file.getName().length() - ext.length() - 1);
	}

	private static void deleteMarkers(final IFile file) {
		try {
			file.deleteMarkers(MARKER_TYPE, false, IResource.DEPTH_ZERO);
		} catch (final CoreException e) {
			e.printStackTrace();
		}
	}

	protected void fullBuild(final IProgressMonitor monitor) {
		try {
			getProject().accept(new RscBundleResourceVisitor());
		} catch (final CoreException e) {
			e.printStackTrace();
		}
	}

	protected void incrementalBuild(final IResourceDelta delta,
			final IProgressMonitor monitor) throws CoreException {
		// the visitor does the work.
		delta.accept(new RscBundleDeltaVisitor());
	}

}
